import { Request, Response, NextFunction } from "express"

const sanitizeString = (str: String) => {
    let newStr = str;

    // TODO: 2 - Remove scripts tags
    // newStr = newStr.replace('script', '')
    // TODO: 3 - Uppercase
    // newStr = newStr.replace('SCRIPT', '')
    // TODO: 4 - First Uppercase
    // newStr = newStr.replace('Script', '')

    // TODO: 5 - Replace angular brackets
    // newStr = newStr.replace('<', '&lt;')
    // newStr = newStr.replace('>', '&gt;');

    return newStr;

};

export const sanitizer = (req: Request, res: Response, next: NextFunction) => {
    Object.entries(req.body)
        .forEach(([key, value]) => {
            if (typeof value === 'string') {
                req.body[key] = sanitizeString(req.body[key]);
            }
        });
    next();
}