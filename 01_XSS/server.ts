import express, { Request, Response } from 'express';
import { comments } from './Data';
import { sanitizer } from './middlewares';

// Init server
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

// TODO: 1 - Apply custom middleware
// app.use(sanitizer);


app.get('/', (req: Request, res: Response) => {
    res.render('index', { comments });
});

app.post('/', (req: Request, res: Response) => {
    const { title, msg, img } = req.body;
    comments.push({
        title,
        msg,
        img
    });

    res.redirect('/');

});

const PORT = 5000;

app.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}/`);
})

