import express, { Request, Response } from 'express';
import { comments } from './Data';

// Init server
const app = express();
app.use(express.json());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req: Request, res: Response) => {
    res.sendFile(`${__dirname}/views/index.html`);
});

app.post('/', (req: Request, res: Response) => {
    const { calc } = req.body;
    const result = eval(calc);
    res.send(`${result}`);

});

const PORT = 5000;

app.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}/`);
})

