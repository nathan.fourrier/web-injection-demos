#!/bin/bash
  
PASS=1234
 
if test -z "${1}"; then
    echo "USAGE : $0 [password]"
    exit 1
fi
 
if test $PASS -eq ${1}; then
    echo "Well done you can validate the password with : $PASS"
else
    echo "Try again ,-)"
fi
 
exit 0